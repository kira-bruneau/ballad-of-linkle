#!/bin/bash

# fail fast
set -e

# make readmes
readme/generate.sh

# make rules
rules/generate.sh

# make output dirs
mkdir -p output/switch/atmosphere/titles/01007EF00011E000/romfs/
mkdir -p output/wiiu/sdcafiine/00050000101C9{4,5}00/Content/
mkdir -p 'output/wiiu/BreathOfTheWild_!LinkleMod_Dialogue/content/'

# copy packs into their places
cp -r ../output/switch/* output/switch/atmosphere/titles/*/romfs/
cp -r ../output/wiiu/* output/wiiu/sdcafiine/00050000101C9400/Content/
cp -r ../output/wiiu/* output/wiiu/sdcafiine/00050000101C9500/Content/
cp -r ../output/wiiu/* 'output/wiiu/BreathOfTheWild_!LinkleMod_Dialogue/content/'

# copy cemu rules
cp rules/rules.txt 'output/wiiu/BreathOfTheWild_!LinkleMod_Dialogue/'

# move the readmes
for platform in switch wiiu; do
  mv readme/readme-"$platform".txt output/"$platform"/readme.txt
done

# make 7z archives... to go inside the zip
cd output/switch
7z a ../switch.7z ./*
cd ../wiiu
7z a ../wiiu.7z ./*
cd ..
rm -rf switch wiiu
cd ..

# get ready for artifacting
mv output /tmp/bol-output
cd /
mv "${CI_PROJECT_DIR}"{,_old}
mkdir "$CI_PROJECT_DIR"
cd "$CI_PROJECT_DIR"
mv /tmp/bol-output/* ./
